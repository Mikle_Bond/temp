/*
 * foo.h
 *
 * Created: 02.04.2016 16:01:07
 *  Author: Mikle_Bond
 */ 


#ifndef FOO_H_
#define FOO_H_

#define LINESHOW_TIME 10

typedef const int8_t (*cfield_t)[16];
typedef int8_t (*field_t)[16];

#include "examples.h"

int8_t field_temp_1[16][16] = {};
int8_t temp[16][16] = {};

int8_t field_bitblock_1[8];
int8_t field_bitblock_2[8];
int8_t field_bitblock_3[8];
int8_t field_bitblock_4[8];

// Not an array, but a pointer;
field_t field_full;

char isTorField = 0;

/******************************************************\
|                  field full                     |    |
| 00                  07 | 08                  15 |    |
|-------------------------------------------------|----|
|                        |                      0 | 00 |
|        block 1         |          block 2       |    |
|                        |                      7 | 07 |
|-------------------------------------------------| -- |
|                        |                      0 | 08 |
|        block 3         |          block 4       |    |
|                        |                      7 | 15 |
\******************************************************/

inline void full2block()
{
	char i = 0, j = 0;
	int8_t word;
	for (i = 0; i < 8; i += 1) {
		// there is no need to set word to zero,
		// it will erase its previous value.
		for (j = 0; j < 8; j += 1) {
			word <<= 1;
			word |= field_full[i][j];
		}
		field_bitblock_1[i] = word;
		for (j = 8; j < 16; j += 1) {
			word <<= 1;
			word |= field_full[i][j];
		}
		field_bitblock_2[i] = word;
	}
	for (i = 8; i < 16; i += 1) {
		for (j = 0; j < 8; j += 1) {
			word <<= 1;
			word |= field_full[i][j];
		}
		field_bitblock_3[i - 8] = word;
		for (j = 8; j < 16; j += 1) {
			word <<= 1;
			word |= field_full[i][j];
		}
		field_bitblock_4[i - 8] = word;
	}
}

#define process_cell(i, j)       \
temp[ i ][j-1] += 1;     \
temp[ i ][j+1] += 1;     \
temp[i-1][j-1] += 1;     \
temp[i-1][ j ] += 1;     \
temp[i-1][j+1] += 1;     \
temp[i+1][j-1] += 1;     \
temp[i+1][ j ] += 1;     \
temp[i+1][j+1] += 1;     \
// define process_cell


inline void update()
{
	int i, j;

	// process each corner
	if (field_full[0][0])
	temp[0][1] = temp[1][0] = temp[1][1] = 1;
	if (field_full[15][15])
	temp[14][15] = temp[15][14] = temp[14][14] = 1;
	if (field_full[0][15])
	temp[0][14] = temp[1][15] = temp[1][14] = 1;
	if (field_full[15][0])
	temp[15][1] = temp[14][0] = temp[14][1] = 1;
	if (isTorField) {
		if (field_full[0][0]) {
			temp[15][0] += 1;
			temp[15][1] += 1;
			temp[0][15] += 1;
			temp[1][15] += 1;
			temp[15][15] += 1;
		}
		if (field_full[0][15]) {
			temp[15][15] += 1;
			temp[15][14] += 1;
			temp[0][0] += 1;
			temp[1][0] += 1;
			temp[15][0] += 1;
		}
		if (field_full[15][0]) {
			temp[0][0] += 1;
			temp[0][1] += 1;
			temp[15][15] += 1;
			temp[14][15] += 1;
			temp[0][15] += 1;
		}
		if (field_full[15][15]) {
			temp[0][15] += 1;
			temp[0][14] += 1;
			temp[15][0] += 1;
			temp[14][0] += 1;
			temp[0][0] += 1;
		}
	}
	

	// process first and last rows
	for (i = 1; i < 15; i += 1) {
		// first row
		if (field_full[0][i]) {
			register char im;
			im = i - 1;
			temp[0][im] += 1;
			temp[1][im] += 1;
			
			temp[1][i] += 1;

			im = i + 1;
			temp[0][im] += 1;
			temp[1][im] += 1;
			if (isTorField) {
				temp[15][i-1] += 1;
				temp[15][ i ] += 1;
				temp[15][i+1] += 1;
			}
		}

		// last row
		if (field_full[15][i]) {
			register char im;
			im = i - 1;
			temp[15][im] += 1;
			temp[14][im] += 1;
			
			temp[14][i] += 1;

			im = i + 1;
			temp[15][im] += 1;
			temp[14][im] += 1;
			if (isTorField) {
				temp[0][i-1] += 1;
				temp[0][ i ] += 1;
				temp[0][i+1] += 1;
			}
		}
	}

	// inner space
	for (i = 1; i < 15; i += 1) {
		// first cell
		if (field_full[i][0]) {
			register char im;
			im = i - 1;
			temp[im][0] += 1;
			temp[im][1] += 1;
			
			temp[i][1] += 1;

			im = i + 1;
			temp[im][0] += 1;
			temp[im][1] += 1;
			if (isTorField) {
				temp[i-1][15] += 1;
				temp[ i ][15] += 1;
				temp[i+1][15] += 1;
			}
		}

		// inner cell
		for (j = 1; j < 15; j += 1) {
			if (field_full[i][j]) {
				process_cell(i, j)
			}
		}

		// last cell
		if (field_full[i][15]) {
			register char im;
			im = i - 1;
			temp[im][15] += 1;
			temp[im][14] += 1;
			
			im = i + 1;
			temp[i][14] += 1;

			temp[im][15] += 1;
			temp[im][14] += 1;
			if (isTorField) {
				temp[i-1][0] += 1;
				temp[ i ][0] += 1;
				temp[i+1][0] += 1;
			}
		}
	}

	for (i = 0; i < 16; i += 1) {
		for (j = 0; j < 16; j += 1) {
			if (temp[i][j] == 3) {
				field_full[i][j] = 1;
				} else if (temp[i][j] != 2) {
				field_full[i][j] = 0;
			}
			temp[i][j] = 0;
		}
	}

	full2block();
}

inline void display()
{
	int i = 0;
	int8_t step = 0x01;
	for (i = 0; i < 8; i++) {
		PORTH.OUT = field_bitblock_1[i];
		PORTK.OUT = field_bitblock_2[i];
		PORTC.OUT = field_bitblock_3[i];
		PORTD.OUT = field_bitblock_4[i];
		PORTJ.OUT = ~step;
		step <<= 1;
		_delay_ms(LINESHOW_TIME);
	}
}

inline void set_preset(cfield_t preset)
{
	int i, j;
	for (i = 0; i < 16; i += 1) {
		for (j = 0; j < 16; j += 1) {
			field_full[i][j] = preset[i][j];
		}
	}
	full2block();
}


inline void next_preset()
{
	static int current_preset = PRESETS_NUM - 1;
	current_preset = (current_preset + 1) % PRESETS_NUM;
	set_preset(get_preset[current_preset]);
	isTorField = get_preset_periodicity[current_preset];
}


#endif /* FOO_H_ */