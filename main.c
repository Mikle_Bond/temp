/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

#include <asf.h>
#define ENABLE_BIT_DEFINITIONS

#include <avr/io.h>
#include <avr/delay.h>
#include <stdint.h>
#include <avr/interrupt.h>

#include "foo.h"

int stepSize = 0;
int step = 0;
int stepSizeArr[4] = { 0, 100, 50, 20 };

static inline void next_step_size()
{
	static char currStepPosition = 3;
	currStepPosition = (currStepPosition + 1) % 4;
	stepSize = stepSizeArr[currStepPosition];
}

inline char isItTimeToUpdate() 
{
	if (!stepSize)
		return 0;

	if (step > stepSize) {
		step = 0;
		return 1;
	} else {
		step += 1;
		return 0;
	}
}

int main(void)
{
	/*can be useful
	PORTCFG.MPCMASK=0x0f;           // ��������� �������� ����������� �������� ����� 0-3
	*/

	/* PORTJ is a source and step controller
	 * PORTK -- 1
	 * PORTH -- 2
	 * PORTC -- 3
	 * PORTD -- 4
	 */

	// initialize ports
	PORTJ.DIR=0xff;
	PORTK.DIR=0x00;
	PORTH.DIR=0x00;
	PORTC.DIR=0x00;
	PORTD.DIR=0x00;
	
	// Include the pull down resister
	PORTA.PIN0CTRL = PORT_OPC_PULLDOWN_gc;
	PORTJ.PIN0CTRL = PORT_OPC_WIREDAND_gc;
	

	// Set the interruption controller to catch the buttons preses
	TCF0.CTRLA = TC_CLKSEL_DIV64_gc;    // Set the timer to run at the fastest rate.
	TCF0.CTRLB = TC_WGMODE_NORMAL_gc;   // Configure the timer for normal counting.
	TCF0.PER = 2048;                    // At 2 MHz, one tick is 0.5 us.  Set period to 8 us.
	TCF0.INTCTRLA = TC_OVFINTLVL_LO_gc; // Configure timer to generate an interrupt on overflow.
	PMIC.CTRL |= PMIC_LOLVLEN_bm;       // Enable this interrupt level.
	sei();
	
	// GAME INITIALIZATIONS
	field_full = field_temp_1; // Field using
	next_preset(); // load the preset
	next_step_size(); // enable step sequencor
	display(); // view the picture for the first time

	while(1) {
		if (isItTimeToUpdate()) {
			update();
		}
		display();
	}
}

// declarations
typedef union {
	int8_t IN;
	struct {
		int B_NONE0:1;
		int B_PRESET:1;
		int B_SPEED:1;
		int B_NONE3:1;
		int B_NONE4:1;
		int B_NONE5:1;
		int B_NONE6:1;
		int B_NONE7:1;
	};
} state_t;
state_t currState = {}, prevState = {}, prevPrevState = {};

#define checkState(name) ( prevPrevState.B_##name ? 0 : (prevPrevState.B_##name = ( prevState.B_##name && currState.B_##name ) ) )

// int32_t count = 0;
/* Function to handle timer overflowing. */
ISR(TCF0_OVF_vect)
{	
	currState.IN = PORTA.IN;
	
	if (checkState(PRESET)) {
		// Button is pressed
		next_preset();
	}
	if (checkState(SPEED)) {
		next_step_size();
	}
	
	prevState.IN = currState.IN;
}
